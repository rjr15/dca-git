#include "comparador.h"
#include <libintl.h>
#include <locale.h>
#include <iostream>
#include <cstdlib>

#define _(x) gettext(x);
using namespace std;

int main (){ 
	setlocale(LC_MESSAGES, "");
    bindtextdomain("p8", ".");

    textdomain("p8");
	Comparador c;
	int mayor = c.mayor(1,3,2);
	cout << gettext("El mayor de 1, 3 y 2 es \n");
	cout << mayor << endl;
	int menor = c.menor(4,5,1);
	cout << gettext("El menor de 4 5 y 1 es \n");
	cout << menor << endl;
	cout << "¡ Gracias por usarme !" << endl;
}
