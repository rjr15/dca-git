# Practica 10 DCA

## Crear algunos alias locales y globales
![Imgur](https://i.imgur.com/VKUjZWs.png)

## Provocar un fallo y encontrarlo con git bisect
![Imgur](https://i.imgur.com/zSZ85XL.png)

## Hooks de git
![Imgur](https://i.imgur.com/eyFC8Qf.png)
